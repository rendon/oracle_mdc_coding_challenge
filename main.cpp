/* Copyright 2017 Rafael Rendón Pablo <rafaelrendonpablo@gmail.com> */
// region Template
#include <bits/stdc++.h>
using namespace std;
typedef long long           int64;
typedef unsigned long long  uint64;
const double kEps   = 10e-8;
const int kMax      = 13400;
const int kInf      = 1 << 30;
// endregion

bool visited[kMax];

struct WordNode {
    string first, last, text;
    int id;
    WordNode() { }
    WordNode(string f, string l, string t, int i) {
        first = f;
        last = l;
        text = t;
        id = i;
    }
};

vector<vector<int>> graph;
vector<WordNode> nodes;

WordNode createNode(const string& text, int id) {
    vector<string> tokens;
    string token;
    for (char c : text) {
        if (c == ' ') {
            tokens.push_back(token);
            token = "";
        } else {
            token += c;
        }
    }
    if (token != "") {
        tokens.push_back(token);
    }
    int n = tokens.size();
    return WordNode(tokens[0], tokens[n-1], text, id);
}

vector<vector<int>> createGraph(vector<WordNode>& nodes) {
    int n = nodes.size();
    vector<vector<int>> graph(n);
    map<string, vector<int>> startWith;
    for (auto node : nodes) {
        startWith[node.first].push_back(node.id);
    }
    for (auto node : nodes) {
        graph[node.id] = startWith[node.last];
    }
    return graph;
}

int dfs(int u) {
    visited[u] = true;
    int len = nodes[u].text.length();

    // Overlap of this title and the next one
    int suffix = nodes[u].last.length();
    int maxLength = 0;
    for (int v : graph[u]) {
        if (!visited[v]) {
            maxLength = std::max(maxLength, dfs(v) - suffix);
        }
    }
    return maxLength + len;
}

int findLargestString() {
    int n = nodes.size();
    int best = 0;
    for (int u = 0; u < n; u++) {
        memset(visited, false, sizeof visited);
        visited[u] = true;
        int x = 0;

        // Overlap of this title and the next one
        int suffix = nodes[u].last.length();
        for (int v : graph[u]) {
            if (!visited[v]) {
                x = std::max(x, dfs(v) - suffix);
            }
        }
        int w = nodes[u].text.length();
        best = std::max(best, x + w);
    }
    return best;
}

int main() {
    ios::sync_with_stdio(false);
    cin.tie(nullptr);
    string line;
    int n = 0;
    while (getline(cin, line)) {
        nodes.push_back(createNode(line, n++));
    }
    graph = createGraph(nodes);
    /*
    for (int i = 0; i < n; i++) {
        cout << nodes[i].text << ": ";
        for (int v : graph[i]) {
            cout << nodes[v].text << endl;
        }
        cout << endl;
    }
    */
    cout << "Largest string: " << findLargestString() << endl;
    return EXIT_SUCCESS;
}
